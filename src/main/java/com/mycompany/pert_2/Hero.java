/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pert_2;

import java.util.*;

/**
 *
 * @author edric
 */
public class Hero {

    public static Scanner scan = new Scanner(System.in);
    static String name;
    static int attack;

    static Specific Warrior = new Specific(200, 50, "Sword", 50, 100, "Warrior");
    static Specific Ranger = new Specific(150, 100, "Bow", 50, 100, "Ranger");
    static Specific Mage = new Specific(100, 200, "Staff", 50, 100, "Mage");
    static Enemy Rocky = new Enemy(300, 20);

    public static void attack() {
        System.out.println("1.Warrior Attack Enemy");
        System.out.println("2.Ranger Attack Enemy");
        System.out.println("3.Mage Attack Enemy");
        attack = scan.nextInt();

        if (attack == 1) {
            while (Rocky.HP > 0 && Warrior.HP > 0) {
                if (Warrior.HP > 0) {
                    Rocky.HP = Rocky.HP - Warrior.Damage;
                    System.out.println("Enemy HP = " + Rocky.HP);
                }
                if (Rocky.HP > 0) {
                    Warrior.HP = Warrior.HP - Rocky.Damage;
                    System.out.println("Warrior HP = " + Warrior.HP);
                }
            }
        } else if (attack == 2) {
            while (Rocky.HP > 0 && Ranger.HP > 0) {
                if (Ranger.HP > 0) {
                    Rocky.HP = Rocky.HP - Ranger.Damage;
                    System.out.println("Enemy HP = " + Rocky.HP);
                }
                if (Rocky.HP > 0) {
                    Ranger.HP = Ranger.HP - Rocky.Damage;
                    System.out.println("Ranger HP = " + Ranger.HP);
                }
            }
        } else if (attack == 3) {
            while (Rocky.HP > 0 && Mage.HP > 0) {
                if (Mage.HP > 0) {
                    Rocky.HP = Rocky.HP - Mage.Damage;
                    System.out.println("Enemy HP = " + Rocky.HP);
                }
                if (Rocky.HP > 0) {
                    Mage.HP = Mage.HP - Rocky.Damage;
                    System.out.println("Mage HP = " + Mage.HP);
                }
            }
        } else {
            System.out.println("Opsi salah");
        }
    }

    public static void main(String[] args) {
        int menu;
        int choose;

        do {
            do {
                System.out.println("Menu");
                System.out.println("1. Choose Job");
                System.out.println("2. Attack");
                System.out.println("3. Exit");
                System.out.print("Your choice [1-3] : ");
                menu = scan.nextInt();
                scan.nextLine();

                switch (menu) {

                    case 1:
                        System.out.println("1. Warrior");
                        System.out.println("2. Ranger");
                        System.out.println("3. Mage");
                        System.out.println("Choose : ");
                        choose = scan.nextInt();
                        switch (choose) {

                            case 1:
                                System.out.print("Name : ");
                                name = scan.nextLine();
                                System.out.println("");
                                System.out.println("Name : " + name);
                                System.out.println("Job : " + Warrior.name);
                                System.out.println("HP : " + Warrior.HP);
                                System.out.println("MP : " + Warrior.MP);
                                System.out.println("Weapon : " + Warrior.Weapon);
                                System.out.println("Damage : " + Warrior.Damage);
                                System.out.println("Crit : " + Warrior.Crit);
                                break;
                            case 2:
                                System.out.print("Name : ");
                                name = scan.nextLine();
                                System.out.println("");
                                System.out.println("Name : " + name);
                                System.out.println("Job : " + Ranger.name);
                                System.out.println("HP : " + Ranger.HP);
                                System.out.println("MP : " + Ranger.MP);
                                System.out.println("Weapon : " + Ranger.Weapon);
                                System.out.println("Damage : " + Ranger.Damage);
                                System.out.println("Crit : " + Ranger.Crit);
                                break;
                            case 3:
                                System.out.print("Name : ");
                                name = scan.nextLine();
                                System.out.println("");
                                System.out.println("Name : " + name);
                                System.out.println("Job : " + Mage.name);
                                System.out.println("HP : " + Mage.HP);
                                System.out.println("MP : " + Mage.MP);
                                System.out.println("Weapon : " + Mage.Weapon);
                                System.out.println("Damage : " + Mage.Damage);
                                System.out.println("Crit : " + Mage.Crit);
                                break;
                        }
                        if (choose < 1 || choose > 3) {
                            break;
                        }

                    case 2:
                        attack();
                        break;
                }

            } while (menu < 1 || menu > 3);
        } while (menu != 3);
    }
}
