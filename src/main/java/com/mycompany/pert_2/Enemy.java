/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pert_2;

/**
 *
 * @author edric
 */
public class Enemy  {
    int HP;
    int Damage;

    public void setHP(int HP) {
        this.HP = HP;
    }

    public void setDamage(int Damage) {
        this.Damage = Damage;
    }

    public int getHP() {
        return HP;
    }

    public int getDamage() {
        return Damage;
    }

    public Enemy(int HP, int Damage) {
        this.HP = HP;
        this.Damage = Damage;
    }
    
    
}
