/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pert_2;

/**
 *
 * @author edric
 */
public class Specific extends Job {

    int HP;
    int MP;
    String Weapon;
    int Damage;
    int Crit;

    public void setHP(int HP) {
        this.HP = HP;
    }

    public void setMP(int MP) {
        this.MP = MP;
    }

    public void setWeapon(String Weapon) {
        this.Weapon = Weapon;
    }

    public void setDamage(int Damage) {
        this.Damage = Damage;
    }

    public void setCrit(int Crit) {
        this.Crit = Crit;
    }

    public int getHP() {
        return HP;
    }

    public int getMP() {
        return MP;
    }

    public String getWeapon() {
        return Weapon;
    }

    public int getDamage() {
        return Damage;
    }

    public int getCrit() {
        return Crit;
    }

    public Specific(int HP, int MP, String Weapon, int Damage, int Crit, String name) {
        super(name);
        this.HP = HP;
        this.MP = MP;
        this.Weapon = Weapon;
        this.Damage = Damage;
        this.Crit = Crit;
    }

    
}
